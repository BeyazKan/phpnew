<?php
/**
 * Created by PhpStorm.
 * User: Anka
 * Date: 8.03.2018
 * Time: 17:17
 */

/**
 * App Sınıfı
 * Temel Router Mantığı
 */
class App
{
    /**
     * @var Sınıfın Özellikleri
     */
    protected $controller;
    protected $method;
    protected $params = [];

    /**
     * App constructor.
     * Sınıf Başladığında yapılacak işlemler
     */
    public function __construct()
    {
        // Varsayılan Değerler
        $this->controller = 'Home';
        $this->method = 'index';

        // Url Çözümleme ve Dahil Etme İşlemleri
        $url = $this->parseUrl();
        if(file_exists('App/Controllers/'.$url[0].'.php')){
            $this->controller = $url[0];
            unset($url[0]);
        }
        require_once 'App/Controllers/'.$this->controller.".php";
        $this->controller = new $this->controller;
        if(isset($url[1])){
            if(method_exists($this->controller, $url[1])){
                $this->method = $url[1];
                unset($url[1]);
            }
        }
        $this->params = $url ? array_values($url) : [];

        /*
         * Sınıf ve Method Çağırma İşlemi (Dinamik olarak Fonksiyon Çalıştıran
         * fonksiyon)
         */
        call_user_func_array([$this->controller, $this->method], $this->params);
    }

    /**
     * $_GET['Url']'i parçalayan method
     * @return array
     */
    private function parseUrl()
    {
        if(isset($_GET['url'])){
            return explode('/', filter_var(rtrim($_GET['url'], '/'),
                FILTER_SANITIZE_URL));
        }
    }
}
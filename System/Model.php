<?php
/**
 * Created by PhpStorm.
 * User: Anka
 * Date: 8.03.2018
 * Time: 17:17
 */

class Model {
    public $db;

    public function __construct()
    {
        require_once 'config/database.php';
        $this->db = database();
    }

    public function fetch($query)
    {
        $q = $this->db->query($query);
        return $q->fetchAll();
    }
}
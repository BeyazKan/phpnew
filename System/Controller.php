<?php
/**
 * Created by PhpStorm.
 * User: Anka
 * Date: 8.03.2018
 * Time: 17:17
 */

/**
 * Controller Sınıfı
 */
class Controller
{
    public function model($model)
    {
        require_once 'App/Models/'.$model.'.php';
        return new $model;
    }

    public function view($view, $data = [])
    {
        require_once 'App/Views/'.$view.'.php';
    }
}
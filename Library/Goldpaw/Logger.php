<?php
    namespace Goldpaw;

class Logger
{

    /**
     * Sınıf Değişkenleri
     */
    private $name;

    /**
    * Sınıf Başlangıç Metodu
    * Şimdilik Sınıfın Başlangıcını Temsil Ediyor.
    */
    public function __construct()
    {
            echo "a class başlangıç<br>";
    }

    /**
    * İsmi getiren fonksiyon
    * @return string isim değişkenini string türünden geri dönüyor.
    */
    public function getName()
    {
        return $this->name;
    }

    /**
    * İsmi alan fonksiyon
    * @param string $name Paramatre olarak vermek isteğiniz ismi yazın.
    */
    public function setName($name)
    {
        $this->name = $name;
    }

    /**
    * Sınıf Bitiş Metodu
    * Şimdilik Sınıfın Bitişini Temsil Ediyor.
    */
    public function __destruct()
    {
        echo "a class bitiş<br>";
    }
}

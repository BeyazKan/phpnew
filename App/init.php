<?php
/**
 * Created by PhpStorm.
 * User: Anka
 * Date: 8.03.2018
 * Time: 17:16
 */

require_once 'vendor/autoload.php';
require_once 'System/App.php';
require_once 'System/Model.php';
require_once 'System/Controller.php';
require_once 'Config/Database.php';
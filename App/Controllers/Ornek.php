<?php
/**
 * Created by PhpStorm.
 * User: Anka
 * Date: 8.03.2018
 * Time: 19:27
 */

class Ornek extends Controller
{
    public function index(){
        echo "Örnek sayfası";
    }

    public function selamla($ad = "s")
    {
        $db = $this->model('OrnekModel');
        $array = [
            "Ad" => $ad,
            "Veri" => $db->get()
        ];
        $this->view('ornek', $array);
    }
}